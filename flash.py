#!/usr/bin/env python3
import subprocess as sp
import sys

def msp430_flash(mote, filename):
    """
    Flash firmware for TelosB motes (MSP430-based)

    Args:
        mote (str): port to flash to
        filename (str): filename of firmware file
    
    Returns:
        Return code of flashing process
    """
    msp430 = (f'./tools/msp430-bsl-linux --telosb -c {mote}')
    try:
        # reset
        p1 = sp.check_call(f'{msp430} -r', shell=True)
        # mass erase
        p2 = sp.check_call(f'{msp430} -e && sleep 2', shell=True)
        # flash
        p3 = sp.check_call(f'{msp430} -I -p {filename} && sleep 2', shell=True)
        # reset
        p4 = sp.check_call(f'{msp430} -r', shell=True)
    except sp.CalledProcessError:
        return 1

    return 0

def cc2538_flash(mote, filename, target_addr=None):
    """
    Flash firmware for Zolertia RE-Motes (CC2538-Based)

    Args:
        mote (str): port to flash to
        filename (str): filename of firmware file
        target_addr (str): start address for flashing (Contiki starts from 
            address 0x00202000 on this platform)

    Returns:
        Return code of flashing process
    """
    args = [f"-p {mote}"]

    if target_addr is not None:
        args.append(f"-a {target_addr}")

    cmd = f'./tools/cc2538-bsl.py -e -w -v -b 460800 {" ".join(args)} {filename}'
    try:
        sp.check_call(cmd, shell=True)
    except sp.CalledProcessError:
        return 1
    return 0

def flash(mote, filename, mote_type, firmware_type):
    """
    Call the appropriate flashing function with the correct arguments 
    for the given firmware and mote

    Args:
        mote (str): port to flash to
        filename (str): filename of firmware file
        mote_type (str): zolertia or telosb
        firmware_type (str): contiki or riot
    
    Raises:
        NotImplementedError: unsupported mote-OS combination
        RuntimeError: unrecognised firmware OS
    """
    if firmware_type == "contiki":
        if mote_type == "zolertia":
            return cc2538_flash(mote, filename, target_addr="0x00202000")
        elif mote_type == "telosb":
            return msp430_flash(mote, filename)
        else:
            raise NotImplementedError(f"Support for flashing {mote_type} for {firmware_type} has not been implemented")
    elif firmware_type == "riot":
        if mote_type == "zolertia":
            return cc2538_flash(mote, filename)
        elif mote_type == "telosb":
            return msp430_flash(mote, filename)
        else:
            raise NotImplementedError(f"Support for flashing {mote_type} for {firmware_type} has not been implemented")
    else:
        raise RuntimeError(f"Unknown firmware type {firmware_type}")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Flash firmware to sensor nodes.')
    parser.add_argument("mote", help="The mote to flash.")
    parser.add_argument("filename", help="The path to the binary to flash.")
    parser.add_argument("mote_type", choices=["zolertia", "telosb"], help="The type of mote.")
    parser.add_argument("firmware_type", choices=["contiki", "riot"], help="The OS that was used to create the firmware.")

    args = parser.parse_args()

    flash(args.mote, args.filename, args.mote_type, args.firmware_type)

