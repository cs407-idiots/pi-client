# Lucidlab testbed observer client

This is the observer code (running on a Raspberry Pi) for the Lucidlab testbed.

## Installation
- Install `git` and `pipenv`: `sudo apt-get install git python3-pip python3-dev pipenv`
- Clone the repository into the Pi's home directory: `git clone https://gitlab.com/cs407-idiots/pi-client.git ~`
- Run `pipenv install` to install Python dependencies

## Running the client
- Run `pipenv shell` to enter a Python shell with the necessary dependencies added to the `PYTHONPATH`.
- Run `python client.py` to start the client. 

## `config.yaml`
A default configuration is provided as `config.yaml.default`. Edit this before running to ensure that the IP address of the server is correct as well as the port of the MQTT broekr and web application (note that this is not the same as the port that the front-end is running on).

# More information
This process is explained in more detail in the [Wiki](https://gitlab.com/cs407-idiots/testbed-server/-/wikis/LucidLab-Setup/LucidLab%20Observer%20Setup%20Instructions) page on observer setup. Some of the information in the other pages may also prove useful.
