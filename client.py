import paho.mqtt.client as mqtt
import sys
import yaml
import requests
from requests.exceptions import HTTPError
import subprocess as sp
import re
import json
import traceback
import glob
import time
import os
from multiprocessing import Process
from datetime import datetime

from flash import flash
from log_output import LogOutput

# Assigned by the server at registration
node_id = -1
# Assigned by the server at job start
job_id = -1
# Stores subprocesses within this file
running_processes = {}
# Stores logging objects
# 1 of these within each entry in running_processes
running_LogOutput = {}
# Set from config.yaml
server_ip = ''
# Location of the *compiled* default firmware
default_firmware_dir = './firmware.default.d/'


def dump_env(env):
    """
    Write out the provided dict to ENV.yaml

    Args:
        env (dict): dict to be written
    """
    with open('ENV.yaml', 'w') as f:
        yaml.dump(env, f)


def register(config, obs_id):
    """
    Register this observer node with the server and ID assigned

    Args:
        config (dict): configuration containing the server IP read
            from config.yaml
        obs_id (int): assigned observer ID from the server

    Raises:
        HTTPError: failed to communicate with server
    
    Returns:
        True if successful
        Otherwise False
    """
    # Performed once we have checked our ID/had it corrected by the server
    try:
        response = requests.get(
            f'http://{server_ip}:{config["server"]["web-port"]}'
            '/register-observer',
            params={
                'observer_id': obs_id
            }
        )

        response.raise_for_status()
        return True
    except HTTPError as e:
        print('Error when registering ID with server: ' + str(e))
        return False


def register_motes(client, id, motelist, interface_list):
    """
    Send an MQTT message to tell the server which motes are connected on
    which ports

    Args:
        client (paho.mqtt.client.Client): MQTT client
        id (int): observer ID
        motelist (list<string>): list of mote serial numbers
        interface_list (list<string>): list of corresponding ports for the motes
    """
    client.publish("topic/register_motes", f"{id};{motelist};{interface_list}")


def send_log(client, job_id, node_id, log, level="INFO"):
    """
    Send a mote log via MQTT, called by the logging callback

    Args:
        client (paho.mqtt.client.Client): MQTT client
        job_id (int): job ID assigned by the server for this test run
        node_id (int): observer ID
        log (LogObject.LogObject): object containing log information
        level (str): information for InfluxDB categorisation
    """
    log_string = (f"{log.time};{job_id};{node_id};"
                  f"{log.serial};{log.mote_id};{level};"
                  f"{log.message}")
    client.publish("topic/log", log_string)


def send_failed_flash(client, mote_id, serial):
    """
    Inform the server that a mote failed to flash. Send both 
    a log to state this and a separate message with a purpose-
    created topic (this is used for recovery)

    Args:
        client (paho.mqtt.client.Client): MQTT client
        mote_id (int): ID of mote that failed to flash
        serial (str): serial number of mote that failed to flash
    """
    client.connect(server_ip)
    time_string = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
    log_string = (f"{time_string};{job_id};{node_id};"
                  f"{serial};{mote_id};ERROR;Mote ID {mote_id} failed to flash")
    client.publish("topic/log", log_string)
    client.publish("topic/failed_flash", f"{mote_id};{node_id}")


def send_ready(client):
    """
    Send an MQTT message informing the server that this observer has
    finished flashing firmware to its connected motes

    Args:
        client (paho.mqtt.client.Client): MQTT client
    """
    client.connect(server_ip)
    client.publish("topic/ready", node_id)


def log_output(client, test_id, logging_object):
    """
    Call logging scripts for running firmware after setting log-sending
    callback functions for normal logs and for default firmware logs

    Args:
        client (paho.mqtt.client.Client): MQTT client
        test_id (int): associated test ID, -2 for default firmware
    """
    # Callback function for normal test logs
    def cb(log):
        print("LOGGING: " + log.message)
        send_log(client, job_id, node_id, log, level="INFO")
    
    # Callback function for default firmware logs
    # Set job_id to -2 to make them distinguishable
    def cb_default(log):
        print("Ping recorded")
        send_log(client, -2, node_id, log, level="INFO")
    
    client.connect(server_ip)
    # Check if default firmware
    if test_id == -2:
        logging_object.callback = cb_default
    else:
        logging_object.callback = cb
    # Instruct the logging object to spawn the logging process
    logging_object.start_logging()


def on_connect(client, userdata, flags, rc):
    """
    Setup the MQTT client on connecting to the broker. Subscribe to
    necessary topics

    Args:
        client (paho.mqtt.client.Client): MQTT client
        userdata: any private data set in the MQTT client (none set in
            this application)
        flags: any flags set by the broker (none set in this application)
        rc (int): result code of connecting to the broker
    """
    print("Connected with result code " + str(rc))
    topics = [
        ("topic/log_output", 0),
        ("topic/start_test", 0),
        ("topic/end_test", 0),
        ("topic/run_default", 0),
        ("topic/run_default_individual", 0)
    ]
    client.subscribe(topics)


def on_unhandled(client, userdata, message):
    """
    Response for unhandled MQTT topics (print and ignore)

    Args:
        client (paho.mqtt.client.Client): MQTT client
        userdata: any private data set in the MQTT client (none set in
            this application)
        message: contents of the MQTT message
    """
    # This shouldn't ever happen, but won't cause a problem if it does
    print("ERROR: Received message on unhandled topic " + message.topic)
    print("Message:")
    print(message.payload.decode())


def on_end_test(client, userdata, message):
    """
    Call function to end the test given by the message

    Args:
        client (paho.mqtt.client.Client): MQTT client
        userdata: any private data set in the MQTT client (none set in
            this application)
        message: contents of the MQTT message (test ID to end)
    """
    test_id = message.payload.decode()
    print(f"Stopping test ID {test_id}")
    end_test(client, test_id)


def end_test(client, test_id):
    """
    End a test given a test ID.
    First figure out which motes that are relevant are connected to this 
    observer using a file listing motes connected. Then terminate each 
    process by placing a sentinel in the managing class. Finally, delete 
    the relevant firmware images and test configuration.

    Args:
        client (paho.mqtt.client.Client): MQTT client
        test_id (int): test ID to end

    Raises:
        FileNotFoundError: when the relevant configuration file is not
            found
    """
    # Attempt to read in the relevant test configuration
    path = f'/home/pi/tests/{test_id}.yaml'
    raw_config = None
    try:
        with open(path) as f:
            raw_config = f.read()
    except FileNotFoundError:
        # If it's not present, we assume that it was never sent to this observer
        # and therefore it's not relevant to this observer (i.e. the configuration
        # mentions none of the connected motes)
        error_string = (f"Configuration for test {test_id} not found")
        print(error_string)
        return
    config = yaml.safe_load(raw_config)
    
    # Read in the file containing mote serial numbers mapped to ports
    connected_motes = None
    with open('motes') as mote_file:
        connected_motes = json.load(mote_file)

    # Build a list of mote serial numbers that must end their test
    to_stop = []
    for image_device in config['image-devices']:
        img_motes = image_device['mote-ids']
        for mote in img_motes:
            if mote['serial'] in connected_motes.keys():
                to_stop.append(connected_motes[mote['serial']])

    # Place a None sentinel in the queue of the logging process
    # This queue normally contains logs to process, but if it sees
    # 'None' it will terminate gracefully
    for mote in to_stop:
        p = running_processes[mote]
        if (p is not None) and p.is_alive():
            running_LogOutput[mote].q.put(None)
            p.join()
            p.terminate()
        running_processes[mote] = None
    
    # Delte every image ID mentioned in the configuration
    for image_device in config['image-devices']:
        img_id = image_device['image-id']
        filename_start = str(img_id) + "-"
        full_filename = ""
        for file in glob.glob(f"/home/pi/images/{filename_start}*"):
            full_filename = file
        sp.call("rm -f " + full_filename, shell=True)
    
    # Delete the test configuration
    sp.call("rm -f " + path, shell=True)


def on_start_test(client, userdata, message):
    """
    Call function to start the test given by the message. Also set 
    the job ID for this run of this test ID

    Args:
        client (paho.mqtt.client.Client): MQTT client
        userdata: any private data set in the MQTT client (none set in
            this application)
        message: contents of the MQTT message (test ID to start)
    """
    global job_id
    test_id, job_id = message.payload.decode().split(';')
    print(f"Beginning test ID {test_id}")
    start_test(client, test_id)


def run_default(motes):
    """
    Flash default firmware to the given motes and begin logging almost 
    identically to normal tests, but set job_id manually to -2 to 
    differentiate logs from the default firmware. Skip any motes that 
    appear to already be running a test (this should only ever be the 
    default firmware)

    Args:
        motes (dict(str: str)): motes to flash as a dict of serial 
        numbers of motes: port
    """
    for serial in motes.keys():
        # If there is already a running process associated with this mote
        # it must be the default firmware, so leave as it is
        if running_processes[motes[serial]] is not None:
            continue
        print("FLASHING DEFAULT FIRMWARE FOR MOTE", serial)
        platform = ""

        # The default firmware is currently built using Contiki-NG
        img_os = 'contiki'

        # Determine what platform this mote is
        # NOTE: This must be updated if more platforms are added
        if serial.startswith('ZOL'):
            platform = 'zolertia'
            flash(motes[serial], default_firmware_dir + 'zolertia-default.hex', platform, img_os)
        else:
            platform = 'telosb'
            flash(motes[serial], default_firmware_dir + 'telosb-default.hex', platform, img_os)
        
        # Create a logging object for the mote
        # We don't know the mote ID because that's ussually sent by the server
        # when starting a test, but that doesn't matter, the server will determine
        # the mote from the serial number
        running_LogOutput[motes[serial]] = LogOutput(motes[serial],
                                                    serial,
                                                    "Unknown",
                                                    platform,
                                                    img_os)
        # Create a process to contain and manage the logging object
        running_processes[motes[serial]] = Process(
            target=log_output,
            args=(mqtt.Client(),
                  -2,
                  running_LogOutput[motes[serial]])
        )
        running_processes[motes[serial]].start()
        

def run_default_all():
    """
    Read in the file containing the connected motes and flash the default 
    firmware to all of them
    """
    connected_motes = None
    with open('motes') as mote_file:
        connected_motes = json.load(mote_file)
    if connected_motes is not None:
        run_default(connected_motes)


def on_run_default(client, userdata, message):
    """
    Call function to run default firmware on every mote on every observer. 
    Should only occur on startup if no tests are scheduled or after a test 
    if no further tests are scheduled

    Args:
        client (paho.mqtt.client.Client): MQTT client
        userdata: any private data set in the MQTT client (none set in
            this application)
        message: contents of the MQTT message (ignored)
    """
    run_default_all()


def on_run_default_individual(client, userdata, message):
    """
    Call function to run default firmware on every mote on a specific 
    observer. Check if that is this observer, then run if it is. Occurs on 
    connection to the server

    Args:
        client (paho.mqtt.client.Client): MQTT client
        userdata: any private data set in the MQTT client (none set in
            this application)
        message: contents of the MQTT message (observer to flash default image)
    """
    run_id = message.payload.decode()
    if int(run_id) == node_id:
        run_default_all()


def start_test(client, test_id):
    """
    Start the test and logging for the test ID given. 
    First read in the configuration to determine which motes that are relevant 
    are connected to this observer. Build a list of tasks detailing which 
    firmware images to flash to which mote and information for logging purposes. 
    Terminate any running tests for those motes (should only be default firmware) 
    and flash the correct image to each relevant mote. Send a ready signal to 
    inform the server that all connected motes have been flashed with the 
    firmware. Finally, call a function to run default firmware on any connected 
    motes that are not involved in this test.

    Args:
        client (paho.mqtt.client.Client): MQTT client
        test_id (int): test ID to start

    Raises:
        FileNotFoundError: when the relevant configuration file is not
            found
    """
    # Attempt to read in the relevant test configuration
    path = f'/home/pi/tests/{test_id}.yaml'
    raw_config = None
    on_this_observer = False
    try:
        with open(path) as f:
            raw_config = f.read()
            on_this_observer = True
    except FileNotFoundError:
        # If it's not present, we assume that it was never sent to this observer
        # and therefore it's not relevant to this observer (i.e. the configuration
        # mentions none of the connected motes)
        error_string = (f"Configuration for test {test_id} not found")
        print(error_string)
        
    # Read in the file containing a mapping of mote serial numbers to ports
    connected_motes = None
    with open('motes') as mote_file:
        connected_motes = json.load(mote_file)

    # Used to determine which motes were not involved in this test and should 
    # have the default firmware flashed to them
    remaining_motes = connected_motes.copy()
    
    # Faslg to report flashing failures
    flash_successful = True
    # If we found the test configuration...
    if on_this_observer:
        # Parse the configuration
        config = yaml.safe_load(raw_config)
        
        # Create a list of 'tasks'
        to_flash = []
        # For every image mentioned in the configuration...
        for image_device in config['image-devices']:
            img_id = image_device['image-id']
            img_motes = image_device['mote-ids']

            # For every mote mentioned for this image...
            for mote in img_motes:

                # If connected to this observer...
                if mote['serial'] in connected_motes.keys():
                    # Remove this mote from the remaining motes that require
                    # default firmware
                    del remaining_motes[mote['serial']]
                    # Add the details necessary for flashing/logging
                    to_flash.append({"serial": mote['serial'],
                                    "mote_id": mote['id'],
                                    "port": connected_motes[mote['serial']],
                                    "img_id": img_id})
        for task in to_flash: 
            # Kill any running process (should only ever be default firmware)
            p = running_processes[task['port']]
            if (p is not None) and p.is_alive():
                running_LogOutput[task['port']].q.put(None)
                p.join()
                p.terminate()
            running_processes[task['port']] = None

            # Read in the compiled firmware
            filename = None
            for file in glob.glob(f"/home/pi/images/{task['img_id']}-*"):
                filename = file

            # Split it- the filename contains information on the OS and platform
            base = os.path.basename(filename)
            base = base.split('.')[0]
            img_os = base.split('-')[1]
            img_platform = base.split('-')[2]
            p = running_processes[task['port']]

            # Flash the firmware to the mote using the dedicated flashing script
            success = flash(task['port'], filename, img_platform, img_os)
            
            # If the return code is not 0, inform the server and stop sttempting to flash
            if success != 0:
                send_failed_flash(client, task['mote_id'], task['serial'])
                flash_successful = False
                break

            # Create a logging object for the mote
            running_LogOutput[task['port']] = LogOutput(task['port'],
                                                        task['serial'],
                                                        task['mote_id'],
                                                        img_platform,
                                                        img_os)
            
            # Create a process to contain and manage the logging object
            running_processes[task['port']] = Process(
                target=log_output,
                args=(mqtt.Client(),
                    test_id,
                    running_LogOutput[task['port']])
            )
            running_processes[task['port']].start()

    # Only tell the server that flashing was successful if there were no errors
    # Also only flash default firmware on success:
    #   The server will either tell every mote to flash default firmware following
    #   this or another test will start, casuing default firmware to flash
    if flash_successful:
        send_ready(client)
        run_default(remaining_motes)


topic_handlers = {
    "topic/start_test": on_start_test,
    "topic/end_test": on_end_test,
    "topic/run_default": on_run_default,
    "topic/run_default_individual": on_run_default_individual
}


def on_message(client, userdata, message):
    """
    Decide which function to call based on the topic of the MQTT message

    Args:
        client (paho.mqtt.client.Client): MQTT client
        userdata: any private data set in the MQTT client (none set in
            this application)
        message: contents of the MQTT message
    
    Raises:
        Exception: error in any of the handler functions
    """
    try:
        topic = message.topic

        callback = topic_handlers.get(topic, on_unhandled)
        callback(client, userdata, message)
    except Exception:
        traceback.print_exc()
        sys.exit(1)


def run():
    """
    Main function.
    
    Load in configuration file and variables in ENV.yaml. Connect to the 
    MQTT broker. Send an HTTP request to the server for ID assignment, if 
    there is an observer ID in ENV.yaml, send this for validation. Write 
    out the received ID.

    Use Perl scripts to determine what motes are connected on which ports 
    and send this information to the server. 

    Raises (in order of appearance in code):
        FileNotFoundError: configuration file not found
        KeyError: missing key in configuration
        Exception: error in MQTT client connection
        Exception: error in acquiring ID from server
    """
    # Read in the configuration file, exit if not found
    config = None
    try:
        with open('config.yaml') as file:
            config = yaml.load(file, Loader=yaml.FullLoader)
    except FileNotFoundError:
        print('config.yaml not found. Please create it '
              'in the current directory and re-run the script.')
        sys.exit(1)

    # ENV.yaml can be created if not found
    env = {}
    try:
        with open('ENV.yaml') as file:
            env = yaml.load(file, Loader=yaml.FullLoader)
    except FileNotFoundError:
        print("ENV.yaml not found; creating...")


    global server_ip
    server_ip = config['server']['ip']

    # ID acquisition
    request_success = False
    if 'id' in env:
        # If we've already got an assigned ID, try to use it
        # Try up to 10 times to connect to the server, sending the current
        # ID as a parameter
        for i in range(10):
            try:
                response = requests.get('http://' + server_ip +
                                        ':8080/request-id?check=' + str(env["id"]))
                response.raise_for_status()
                result = int(response.content)
                # Use whatever the server says regardless
                if result == env['id']:
                    print(f'Using ID {env["id"]} from ENV.yaml.')
                    request_success = True
                    break
                else:
                    print(f'Allocated new ID {result} from server.')
                    env['id'] = result
                    dump_env(env)
                    request_success = True
                    break
            except requests.exceptions.ConnectionError:
                print("Failed to connect to server, trying again in 10 seconds... "
                    f"({10-i-1} attempt(s) left)")
                time.sleep(10)
            except Exception as e:
                print("Error while acquiring ID from server")
                raise
    else:
        # Same process as above but don't send any parameters
        for i in range(10):
            try:
                response = requests.get('http://' + server_ip +
                                        ':8080/request-id')
                response.raise_for_status()
                result = int(response.content)
                print(f'Allocated new ID {result} from server.')
                env['id'] = result
                dump_env(env)
                request_success = True
                break
            except requests.exceptions.ConnectionError:
                print("Failed to connect to server, trying again in 10 seconds... "
                        f"({10-i-1} attempt(s) left)")
                time.sleep(10)
            except Exception as e:
                print("Error while acquiring ID from server")
                raise

    # Assume that a continued failure is just a connection refused error
    # as any other exception is caught and raised
    if not request_success:
        raise ConnectionRefusedError("Could not connect to server")

    global node_id
    node_id = env['id']

    # Use the assigned ID to register with the server
    success = register(config, node_id)
    while not success:
        # failed to register; try again 1s later
        print('Error in registration, trying again...')
        time.sleep(1)
        success = register(config, node_id)

    # Necessary during tests to ensure the MQTT client connects properly
    # May be possible to remove on systems more powerful than an RPi
    time.sleep(2)
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    # Try to connect to the broker up to 10 times
    for i in range(10):
        try:
            client.connect(server_ip,
                        config['server']['mqtt-port'],
                        5)
            break
        except KeyError as k:
            print(f"Invalid config: expected key {k.args[0]}")
            sys.exit(1)
        except Exception as e:
            print("Failed to connect to MQTT broker, trying again in 10 seconds... "
                 f"({10-i-1} attempts left)")
            time.sleep(10)

    # Mote detection:
    # Use existing Perl scripts to find out what motes are connected on which
    # ports
    # NOTE: This will need updating to account for other platforms
    motes_zolertia = sp.check_output('./tools/motelist-zolertia', shell=True)
    motelist_zolertia = re.findall(r'ZOL[\-A-Z\d]+', str(motes_zolertia))
    interface_list_zolertia = re.findall(r'\/dev\/ttyUSB\d+',
                                         str(motes_zolertia))
    motes_telosb = sp.check_output('./tools/motelist-sky', shell=True)
    motelist_telosb = re.findall(r'\\n\w+', str(motes_telosb))
    motelist_telosb = [i.strip("\\n") for i in motelist_telosb]
    interface_list_telosb = re.findall(r'\/dev\/ttyUSB\d+', str(motes_telosb))

    motelist = []
    motelist.extend(motelist_zolertia)
    motelist.extend(motelist_telosb)
    interface_list = []
    interface_list.extend(interface_list_zolertia)
    interface_list.extend(interface_list_telosb)
    print("Connected motes:")
    print(motelist)
    print("On ports")
    print(interface_list)
    mote_register = dict(zip(motelist, interface_list))

    # Initialise running process list to ensure correct default values
    for port in interface_list:
        running_processes[port] = None

    # Write out connected motes and ports
    with open('motes', 'w') as file:
        file.write(json.dumps(mote_register))

    # Inform the server which motes are connected to which ports
    register_motes(client, node_id, str(motelist), str(interface_list))

    # Now just listen for MQTT messages
    client.loop_forever()


if __name__ == '__main__':
    run()
