#!/usr/bin/python

from multiprocessing import Process, active_children
import subprocess
import sys
from multiprocessing import Queue
import json
from datetime import datetime
from LogObject import LogObject
import os
import signal

class LogOutput:
    """
    Performs logging for a specific mote through a consumer-producer paradigm

    Spawns a separate process for the producer, which the spawns a subprocess that 
    runs the logging script. Termination occurs through a sentinel value of None 
    in the output queue. This ensure proper closing of the logging script to prevent 
    issues with unclosed file descriptors for the mote port.

    Attributes:
        port (str): port that the mote is connected to
        serial (str): serial number of the mote (for logging)
        mote_id (int): server-assigned ID for the mote
        callback (Callable): function to send logging output to
        q (multiprocessing.Queue): intermediate storage for logging output
        p (multiprocessing.Process): producer process
        sub_p (subprocess.Popen): logging script subprocess
        platform (str): zolertia or telosb
        firmware_type (str): contiki or riot
    """
    def __init__(self, port, serial, mote_id, platform='zolertia', firmware_type='contiki', callback=None):
        self.port = port
        self.serial = serial
        self.mote_id = mote_id
        self.callback = callback
        self._TERMINATE= False
        self.q = Queue()
        self.p = None
        self.sub_p = None
        self.platform = platform
        self.firmware_type = firmware_type

    def run(self, baud='115200'):
        """
        Producer process that calls the logging script. Timestamps 
        output and placess it in the queue for interprocess communication 

        Args:
            baud (str): logging baud rate
        
        Raises:
            subprocess.CalledProcessError: when logging script fails
        """
        pstring = (f'tools/pyterm -b{baud} -p {self.port}')
        # Necessary to redirect stdin to ensure termination will work
        self.sub_p = subprocess.Popen(pstring, stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell=True)
        with self.sub_p as p:
            # Every time the logging process outputs to stdout, create a LogObject and add it to the queue
            for line in p.stdout:
                time_string = datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%f")
                self.q.put(LogObject(str(time_string), str(self.serial), str(self.mote_id), str(line)))

        if p.returncode != 0:
            raise subprocess.CalledProcessError(p.returncode, p.args)

    def start_logging(self):
        """
        Consumer process. Spawns a process to call the logging script. 
        Consumes logging output through a queue and then sends them to a 
        callback function which will send them to the server. On detection 
        of a None sentinel in the queue (placed when ending a test), kill 
        the producer process

        Setting p.daemon = True ensures the logging script process is killed
        """
        # Specific case:
        if self.firmware_type == 'riot' and self.platform == 'telosb':
            baud = '9600'
        else:
            baud = '115200'
        try:
            self.p = Process(target=self.run, args=(baud,))
            # Necessary to ensure that the producer process will kill the logging subprocess
            self.p.daemon = True
            self.p.start()
            while True:
                # Continually get from the queue and send to the callback
                # Look out for None sentinel, telling the process to terminate
                while not self.q.empty():
                    v = self.q.get()
                    if v is None:
                        self.p.kill()
                        return
                    if self.callback is not None:
                        self.callback(v)
                    else:
                        print(v.dump())

        except KeyboardInterrupt:
            print("exit")
            print(self.q.qsize())
            sys.exit(0)
