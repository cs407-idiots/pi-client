class LogObject:
    """
    Simple class to neatly encapsulate the data for a log

    Attributes:
        time (datetime.datetime): log timestamp
        serial (string): serial number of the source mote
        mote_id (string): server-assigned ID of the source mote
        message (string): log output
    """
    def __init__(self, time, serial, mote_id, message):
        self.time = time
        self.serial = serial
        self.mote_id = mote_id
        self.message = message

    def get_contents(self):
        """
        Return contents of log as a dict

        Returns:
            dict of attributes
        """
        return {'time': self.time,
                'serial': self.serial,
                'mote_id': self.mote_id,
                'message': self.message}

    def dump(self):
        """
        Return contents of log as a string

        Returns:
            String of attributes separated by ';'
        """
        return self.time + ';' + self.serial + ';' + self.mote_id + ';' + self.message
