/* Modified and reproduced by Scott Pickering in 2020 <s.pickering.2@warwick.ac.uk>
 * Formed of a culmination of two pieces of firmware: Contiki's example broadcast
 * software and software to evaluate an RF environment using CCA.
 * 
 * The following copyrights are from the original code and apply to this firmware.
 *
 * === SECTIONS RELATING TO CCA ===
 * 
 * Copyright (c) 2016, Robert Olsson KTH Royal Institute of Technology
 * COS/Kista Stockholm roolss@kth.se
 *
 * All rights reserved.
 * 
 * === SECTIONS RELATED TO BROADCASTING ===
 * 
 * Copyright (c) 2007, Swedish Institute of Computer Science.
 * All rights reserved.
 * 
 * === LICENSE TERMS ===
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "contiki.h"
#include "net/nullnet/nullnet.h"
#include "net/link-stats.h"
#include "random.h"
#include "dev/radio.h"
#include "net/netstack.h"
#include "net/packetbuf.h"
#include "sys/process.h"
#include "sys/etimer.h"

#include "dev/leds.h"
#include "dev/button-sensor.h"

#include <stdio.h>
#include <string.h>

#define SAMPLES 1000

PROCESS(default_firmware, "default_firmware");
AUTOSTART_PROCESSES(&default_firmware);

static int cca[16], noise_floor[16], i, j;

/*
** === CCA FUNCTIONS === 
*/
static void
set_chan(uint8_t chan)
{
  // Set the 802.15.4 2.4GHz radio channel
  if(NETSTACK_RADIO.set_value(RADIO_PARAM_CHANNEL, chan) ==
     RADIO_RESULT_OK) {
  }
}
 
void 
do_all_chan_stats(int *cca)
{
  // For each channel...
  for(j = 0; j < 16; j++) {
    set_chan(j+11);
    cca[j] = 0;
    if (NETSTACK_RADIO.get_value(RADIO_PARAM_RSSI, &noise_floor[j]) != RADIO_RESULT_OK){
      noise_floor[j] = 0;
    }
    // ...Perform 1000 CCAs
    for(i = 0; i < SAMPLES; i++) {
      cca[j] += NETSTACK_RADIO.channel_clear();
    }
  }
}

// Redefine callback on received message
void input_callback(const void *data, uint16_t len,
  const linkaddr_t *src, const linkaddr_t *dest)
{
  // Print out the address of this mote
  // (Used by the mote monitor)
  printf("my_addr:");
  int i;
  for (i = 0; i < LINKADDR_SIZE; i++){
    printf("%d", linkaddr_node_addr.u8[i]);
    if (i < LINKADDR_SIZE - 1){
      printf(".");
    }
  }
  // Print out the source of the broadcast
  printf(";src_addr:");
  for (i = 0; i < LINKADDR_SIZE; i++){
    printf("%d", src->u8[i]);
    if (i < LINKADDR_SIZE - 1){
      printf(".");
    }
  }
  // Print out the last value in the RSSI register
  // Hopefully this corresponds to receiving the last Tx
  printf(";rssi:%d\n",(signed short)packetbuf_attr(PACKETBUF_ATTR_RSSI));
}

PROCESS_THREAD(default_firmware, ev, data)
{
  static struct etimer b_cast_timer;
  static struct etimer cca_timer;

  PROCESS_BEGIN();

  // Arbitrary message to broadcast
  char msg[9] = "LucidLab";
  nullnet_buf = (unsigned char *)msg;

  // Set the callback on receiving a message
  nullnet_set_input_callback(input_callback);

  // Create 2 timers: 1 to broadcast (more frequently), 1 to perform CCA (less frequently)
  etimer_set(&b_cast_timer, CLOCK_SECOND * 4 + random_rand() % (CLOCK_SECOND * 4));
  etimer_set(&cca_timer, CLOCK_SECOND * 40 + random_rand() % (CLOCK_SECOND * 10));
  while(1){
    // Wait for at least 1 timer to expire
    PROCESS_WAIT_EVENT();

    if (etimer_expired(&b_cast_timer)){
      // Set the Tx buffer
      nullnet_buf = (unsigned char *)msg;
      nullnet_len = strlen(msg) * sizeof(char);
      // Broadcast
      NETSTACK_NETWORK.output(NULL);
      etimer_reset(&b_cast_timer);
    } else 
    if (etimer_expired(&cca_timer))
    {
      // Perform stats and print out the result for each channel
      do_all_chan_stats(cca);
      printf("cca_res:");
      for (i=0; i < 16; i++){
        printf("%d", cca[i]);
        if (i < 15){
          printf(";");
        }
      }
      printf("\n");

      printf("noise_floor_res:");
      for (i=0; i < 16; i++){
        printf("%d", noise_floor[i]);
        if (i < 15){
          printf(";");
        }
      }
      printf("\n");
      etimer_reset(&cca_timer);
    }
    
  }

  PROCESS_END();
}